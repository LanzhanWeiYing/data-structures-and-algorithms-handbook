# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->


# Aspiring To be Everything in the world 

## Kristel Magpayo

👋 Aspiring IT specialist/ Consultant! 🚀✨⚡🔥 — 💌 krystelmagpayo@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_magpayo_kristel.jpg](images\act_2_magpayo_kristel.jpg)
### Bionpm

**Good to know:** Persistence is one trait that I describe for myself. I am resilient and not afraid to get my hands dirty if needed to be. I have many interests, but I tend to be impulsive and not finish what I start. I also love to Travel and have adventures.

**Motto:** Idc about people, I care about other stuff--n'd being sexy**

**Languages:** Python, Java, html

**Other Technologies:** VSCODE, Eclipse, MS Office, basic linux software, Pycharm, clipchamp 

**Personality Type:** [Protagonist (ENTJ-T)](https://www.16personalities.com/profiles/3ad837eacf5eb)

<!-- END -->
